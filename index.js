const Logger = require('./Logger');

const Shopper = require('./Shopper');

const Store = require('./Store');


const logger = new Logger().getInstance();


logger.log('App is starting up!!!');


const alex = new Shopper('Alex',500);

const code_shop = new Store('Node.js Scripts', [
    {
        item: 1,
        price: 50
    },
    {
        item: 2,
        price: 90
    }
]);

logger.log('Finished config');

console.log(`Total log: ${logger.count}`);

logger.logs.map(log => console.log(log));


